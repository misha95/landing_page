
$(document).on('ready', function() {
	$(".slider").slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		variableWidth: true,
		nextArrow: '<div class="slider_elements icon-right"></div>',
		prevArrow: '<div class="slider_elements icon-left"></div>'
	});	
});
